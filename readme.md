Firstly generate the private key and self-signed root certificate

    ./init_ca.sh

Then setup the CA service in central server by uploading the CA public certificate ca.pem.
For the certificate profile you may select 

    ee.ria.xroad.common.certificateprofile.impl.EjbcaCertificateProfileInfoProvider

For signing auth/sign csr's, run

    ./process_csr.sh <csr filename>

which will generate the cert into the newcerts folder.
NB a sign cert is created if the filename starts with 'sign', same story with 'auth'.

#!/bin/bash

set -ex

openssl req -config ca_config.cfg -newkey rsa:2048 -nodes -keyout ca_key.pem -x509 -out ca.pem

mkdir newcerts 2> /dev/null
> index.txt
echo '00' > serial

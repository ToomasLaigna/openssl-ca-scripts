#!/bin/bash

set -ex

csr_filename="$1"
if [[ ${csr_filename} == sign* ]]; then
    keyUsage=sign
else
    keyUsage=auth
fi

openssl ca -config ca.cfg -extfile x509ext."${keyUsage}".cfg -in "${csr_filename}"
